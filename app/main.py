import os

from fastapi import FastAPI
from fastapi.responses import RedirectResponse

app = FastAPI(
    title="Playgrounds API",
    description="Simple API",
    version=os.environ.get("API_VERSION", "dev")
)


@app.get("/hw")
async def hello_world():
    return {"message": "hello world"}


@app.get("/", include_in_schema=False)
async def redirect_docs():
    return RedirectResponse(url="/docs")
