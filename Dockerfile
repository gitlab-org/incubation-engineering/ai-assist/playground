FROM python:3.10

WORKDIR /app

# Should be COPY
ADD requirements.txt .

# Should add --no-cache
RUN pip install -r requirements.txt

# Should be COPY app app
COPY app app

COPY requirements.txt requirements.txt

ENV API_PORT 8001
EXPOSE $API_PORT

# Should be JSON annotation
CMD uvicorn app.main:app --host 0.0.0.0 --port $API_PORT
